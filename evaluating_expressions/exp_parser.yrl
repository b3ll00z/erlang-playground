Nonterminals exp.
Terminals num open close plus minus times quot unary 'if' 'then' 'else'.
Rootsymbol exp.

exp -> num : {num, extract_token('$1')}.
exp -> open exp close : '$2'.
exp -> exp plus exp : {plus, '$1', '$3'}.  
exp -> exp minus exp : {minus, '$1', '$3'}. 
exp -> exp times exp : {times, '$1', '$3'}.
exp -> exp quot exp : {quot, '$1', '$3'}. 
exp -> unary exp : {unary, '$2'}. 
exp -> 'if' exp 'then' exp 'else' exp : {if_then_else, '$2', '$4', '$6'}.

Erlang code.

extract_token({_Token, _Line, Value}) -> Value.
