This exercise asks you to build a collection of functions that manipulate 
arithmetical expressions. 

Start with an expression such as the following: 
* ((2+3)-4) 
* 4 
* ~((2\*3)+(3\*4)) 

which is fully bracketed and where you use a tilde (~) for unary minus.

First, write a parser for these, turning them into Erlang representations, such 
as the following: 

	{minus, {plus, {num, 2}, {num,3}}, {num, 4}} 

which represents ((2+3)-4). We call these exp s. Now, write an evaluator, which takes an exp and 
returns its value.

You can also extend the collection of expressions to add conditionals: 

	if ((2+3)-4) then 4 else ~((2*3)+(3*4))
    
where the value returned is the “then” value if the “if” expression evaluates to
0, and it is the “else” value otherwise.

_To solve this exercises I found out that Erlang environment offers two modules for lexical and syntax analysis. So in **exp_lexical_analyzer.xrl** there are my specifications for tokens recognition and in **exp_parser.yrl** the production rules for the parsing phase of tokens given by lexical analyzer._

	1> leex:file('exp_lexical_analyzer.xrl').
    2> c(exp_lexical_analyzer).
    3> yecc:file('exp_parser.yrl').
    4> c(exp_parser).
    5> c(exp).
    6> X = exp:exp("if ((2+3)-4) then 4 else ~((2*3)+(3*4))").
    7> exp:eval(X).

_The block above is a simple demo of final work._

