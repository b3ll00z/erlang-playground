-module(exp).
-import(exp_lexical_analyzer, [string/1]).
-import(exp_parser, [parse/1]).
-export([exp/1, eval/1]).

exp(S) ->
	{ok, Tokens, _} = string(S),
	{_, Expression} = parse(Tokens),
	Expression.

eval({num, N}) -> N;
eval({plus, L, R}) -> eval(L) + eval(R);
eval({minus, L, R}) -> eval(L) - eval(R);
eval({times, L, R}) -> eval(L) * eval(R);
eval({quot, L, R}) -> eval(L) / eval(R);
eval({unary, E}) -> -eval(E);
eval({if_then_else, C, T, E}) -> if_then_else(eval(C) == 0, T, E).

if_then_else(true, THEN, _) -> eval(THEN);
if_then_else(false, _, ELSE) -> eval(ELSE). 
