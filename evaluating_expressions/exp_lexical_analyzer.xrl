Definitions.

INT 		= [0-9]+
WHITESPACE	= [\s\t\n\r]

Rules.

{INT}		  : {token, {num, TokenLine, list_to_integer(TokenChars)}}.
\(			  : {token, {open, TokenLine}}.
\)			  : {token, {close, TokenLine}}.
\+			  :	{token, {plus, TokenLine}}.
-			  :	{token, {minus, TokenLine}}.
\*			  :	{token, {times, TokenLine}}.
\/			  : {token, {quot, TokenLine}}.
\~			  :	{token, {unary, TokenLine}}.
if			  : {token, {'if', TokenLine}}.
then		  : {token, {'then', TokenLine}}.
else		  : {token, {'else', TokenLine}}.
{WHITESPACE}+ : skip_token.

Erlang code.
