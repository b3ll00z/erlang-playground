-module(sequential).
-export([is_palindrome/1, is_an_anagram/1, is_prime/1, is_proper/1]).

% Define the following functions in Erlang:

% is_palindrome: string -> bool that checks if the string given as input is palindrome, a
% string is palindrome when the represented sentence can be read the same way in either 
% directions in spite of spaces, punctual and letter cases, 
% e.g., detartrated, "Do geese see God?", "Rise to vote, sir.", ...

remove_spurious(X) -> [Y || Y <- string:to_upper(X), Y >= $A, Y =< $Z].

is_palindrome(X) -> remove_spurious(X) == remove_spurious(string:reverse(X)).

% is_an_anagram : string -> string list -> boolean, that given a dictionary of strings, 
% checks if the input string is an anagram of one or more of the strings in the dictionary;

is_an_anagram(X, L) -> 
	S = lists:sort(remove_spurious(X)),
	F = fun(W,Z) -> W or Z end,
	lists:foldl(F, false, lists:map(fun(Y) -> S == lists:sort(remove_spurious(Y)) end, L)).

% factors: int -> int list, that given a number calculates all its prime factors;
is_prime(N) -> length([X || X <- lists:seq(2, trunc(math:sqrt(N))), ((N rem X) == 0)]) == 0.

prime_divisors(N) -> [X || X <- lists:seq(2, N), ((N rem X) == 0), is_prime(X)].

factors(N) -> factors(N, prime_divisors(N)).

factors(_, []) -> [];
factors(N, [H|TL]) when (N rem H) == 0 -> [H | factors(N div H, [H|TL])];
factors(N, [_|TL]) -> factors(N, TL).

% is_proper: int -> boolean, that given a number calculates if it is a perfect number or not,
% where a perfect number is a positive integer equal to the sum of its proper positive divisors
% (excluding itself)
% e.g., 6 is a perfect number since 1, 2 and 3 are the proper divisors of 6 and 6 is equal to 1+2+3;

is_proper(N) -> 
	D = [X || X <- lists:seq(1, N-1), ((N rem X) == 0)], 
	lists:foldl(fun(X, Y) -> X + Y end, 0, D) == N.
