-module(ring_v1).
-export([start/3, wait_next/0, log/4]).

start(M, N, Message) -> create_ring(M, N, Message, []).

create_ring(M, 0, Message, TL) -> 
	io:format("End of ring creation~n"),
	connect_edges(M, Message, TL ++ [lists:nth(1, TL)]);
	
create_ring(M, N, Message, TL) ->
	Pid = spawn(ring_v1, wait_next, []),
	io:format("Process ~p created ~n", [Pid]),
	create_ring(M, N-1, Message, TL ++ [Pid]).
	

connect_edges(_, Message, [X]) -> 
	io:format("Edges connected! Let's start from ~p~n", [X]),
	X ! {self(), Message};

connect_edges(M, Message, [X, Y | TL]) ->
	X ! {Y, M},
	connect_edges(M, Message, [Y | TL]).
	

wait_next() -> 
	receive
		{Next, Times} -> 
			io:format("~p will send ~p times a message to ~p~n", [self(), Times, Next]),
			loop(Next, Times-1)
	end.
	
	
log(Message, From, Next, M) ->
	Me = self(),
	io:format("~n[~p]: received ~p from ~p.~n", [Me, Message, From]),
	io:format("[~p]: Let's write to ~p~n", [Me, Next]),
	io:format("[~p]: ~p messages to send.~n", [Me, M]).	
	
loop(Next, 0) ->
	receive
		{From, Message} ->
			log(Message, From, Next, 0),
			Next ! {self(), Message},
			io:format("[~p]: goodbye!~n", [self()])
	end;
	
loop(Next, M) ->
	receive
		{From, Message} ->
			log(Message, From, Next, M),
			Next ! {self(), Message},
			loop(Next, M-1)
	end.
