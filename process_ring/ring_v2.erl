-module(ring_v2).
-import(ring_v1, [log/4]).
-export([start/3, create_ring/4]).

start(M, N, Message) -> create_ring(M, N-1, Message, self()).

create_ring(M, 0, Message, First) ->
	io:format("~p will send ~p times a message to ~p~n", [self(), M, First]),
	loop(First, M-1, Message);		
	
create_ring(M, N, Message, First) ->
	Pid = spawn(ring_v2, create_ring, [M, N-1, Message, First]),
	io:format("~p will send ~p times a message to ~p~n", [self(), M, Pid]),
	loop(Pid, M-1).
	
loop(Next, 0) ->
	receive
		{From, Message} ->
			log(Message, From, Next, 0),
			Next ! {self(), Message},
			io:format("[~p]: goodbye!~n", [self()])
	end;
	
loop(Next, M) ->
	receive
		{From, Message} ->
			log(Message, From, Next, M),
			Next ! {self(), Message},
			loop(Next, M-1)
	end.
	
loop(Next, M, Message) ->
	Me = self(),
	io:format("~n[~p]: I am the starter!~n", [Me]),
	io:format("[~p]: Let's write to ~p~n", [Me, Next]),
	Next ! {Me, Message}, 
	io:format("[~p]: ~p messages to send.~n", [Me, M]),
	loop(Next, M-1).
