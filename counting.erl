% Write a module counting which provides the functionality for interacting with 
% a server that counts how many times its services has been requested.
% It has to implement several services dummy1, ... dummyn (doesn't matter what 
% they do or their real interface) and a service tot that returns a list of 
% records indexed on each service (tot included) containing also how many times 
% such a service has been requested. Test it from the shell.

-module(counting).
-export([sum/2, times/2, sub/2, tot/0]).

updater(X) -> counter_checker(whereis(counter), X).

counter_checker(undefined, X) -> 
	Counts = [{sum, 0}, {sub, 0}, {times, 0}, {tot, 0}],
	register(counter, spawn(fun() -> count(Counts) end)), 
	whereis(counter) ! X;
	
counter_checker(Pid, X) -> Pid ! X.

refresh(X, L) -> 
	{[{_, CountX}], Others} = lists:partition(fun({Y, _}) -> X == Y end, L),
	[{X, CountX + 1}] ++ Others.

count(L) ->
	receive
		sum -> count(refresh(sum, L));
		times -> count(refresh(times, L));
		sub -> count(refresh(sub, L));
		tot -> count(refresh(tot, L));
		{counts, From} -> From ! {counts, L}, count(L)
	end.

sum(X, Y) -> io:format("~p + ~p = ~p~n", [X, Y, X + Y]), updater(sum).

times(X, Y) -> io:format("~p * ~p = ~p~n", [X, Y, X * Y]), updater(times).

sub(X, Y) -> io:format("~p - ~p = ~p~n", [X, Y, X - Y]), updater(sub).

tot() -> 
	updater(tot), 
	whereis(counter) ! {counts, self()}, 
	receive 
		{counts, X} -> X 
	end.
